function searchById(inventory, searchId) {
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id === searchId) { // find the searchId and return it to where it is called
            return inventory[i];
        }
    }
}
module.exports = searchById;

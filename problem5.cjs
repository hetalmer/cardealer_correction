function olderCar(carList) {
    let oldCar = [];
    for (let i = 0; i < carList.length; i++) {
        if (carList[i] < 2000) { // get the car which is older than 2000
            oldCar.push(carList[i])
        }
    }
    return oldCar;
}
module.exports = olderCar;

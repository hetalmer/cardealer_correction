
//This program is to get the information of car which car id is 33.
const searchById = require('../problem1.cjs');
//get the information of inventory form json file
let inventory = require('../inventory.json');
output = searchById(inventory, 33);
console.log(`car 33 is *${output.car_year}* *${output.car_make}* *${output.car_model}*`);
